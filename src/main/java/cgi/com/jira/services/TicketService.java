package cgi.com.jira.services;

import cgi.com.jira.persistance.entities.TicketEntity;
import org.cgi.ws.Ticket;

public interface TicketService {

    Ticket getTicketById(Long id);

    Ticket getTicketByName(String name);

    TicketEntity addTicket(TicketEntity ticket);
}