package cgi.com.jira.services;

import cgi.com.jira.persistance.entities.TicketEntity;
import cgi.com.jira.persistance.repository.TicketRepository;
import org.cgi.ws.Ticket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketServiceImpl implements TicketService {
    private TicketRepository ticketRepository;

    @Autowired
    public TicketServiceImpl(TicketRepository ticketRepository) {
        this.ticketRepository = ticketRepository;
    }

    @Override
    public Ticket getTicketById(Long id) {
        TicketEntity ticketEntity = ticketRepository.getTicketById(id);

        Ticket ticketById = new Ticket();
        ticketById.setId(ticketEntity.getId());
        ticketById.setName(ticketEntity.getName());
        ticketById.setEmail(ticketEntity.getEmail());
        ticketById.setIdPersonCreator(ticketEntity.getIdPersonCreator());
        ticketById.setIdPersonAssigned(ticketEntity.getIdPersonAssigned());
        ticketById.setCreationDatetime(ticketEntity.getCreationDateTime());
        ticketById.setTicketCloseDatetime(ticketEntity.getTicketCloseDateTime());

        return ticketById;
    }

    @Override
    public Ticket getTicketByName(String name) {
        TicketEntity ticketEntity = ticketRepository.getTicketByName(name);

        Ticket ticketByName = new Ticket();
        ticketByName.setId(ticketEntity.getId());
        ticketByName.setName(ticketEntity.getName());
        ticketByName.setEmail(ticketEntity.getEmail());
        ticketByName.setIdPersonCreator(ticketEntity.getIdPersonCreator());
        ticketByName.setIdPersonAssigned(ticketEntity.getIdPersonAssigned());
        ticketByName.setCreationDatetime(ticketEntity.getCreationDateTime());
        ticketByName.setTicketCloseDatetime(ticketEntity.getTicketCloseDateTime());

        return ticketByName;
    }

    public TicketEntity addTicket(TicketEntity ticket) {
        if (ticketRepository.newTicket(ticket) != 0) {
            return ticket;
        } else {
            return null;
        }
    }
}