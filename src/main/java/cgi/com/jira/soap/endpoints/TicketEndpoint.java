package cgi.com.jira.soap.endpoints;

import cgi.com.jira.persistance.entities.TicketEntity;
import cgi.com.jira.services.TicketService;
import org.cgi.ws.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class TicketEndpoint {

    private static final String NAMESPACE_URI = "http://cgi.org/ws";
    private TicketService ticketService;

    @Autowired
    public TicketEndpoint(TicketService ticketService) {
        this.ticketService = ticketService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByIdRequest")
    @ResponsePayload
    public GetTicketByIdResponse getTicketById(@RequestPayload GetTicketByIdRequest request) {
        GetTicketByIdResponse getTicketByIdResponse = new GetTicketByIdResponse();
        getTicketByIdResponse.setTicket(ticketService.getTicketById(request.getId()));
        return getTicketByIdResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetTicketByNameRequest")
    @ResponsePayload
    public GetTicketByNameResponse getTicketByName(@RequestPayload GetTicketByNameRequest request) {
        GetTicketByNameResponse getTicketByNameResponse = new GetTicketByNameResponse();
        getTicketByNameResponse.setTicket(ticketService.getTicketByName(request.getName()));
        return getTicketByNameResponse;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "AddNewTicketRequest")
    @ResponsePayload
    public AddNewTicketResponse addTicketResponse(@RequestPayload AddNewTicketRequest request) {
        AddNewTicketResponse returnInfo = new AddNewTicketResponse();
        Ticket ticket = new Ticket();
        TicketEntity obtainedInfo = new TicketEntity(
                request.getTicket().getId(),
                request.getTicket().getName(),
                request.getTicket().getEmail(),
                request.getTicket().getIdPersonAssigned(),
                request.getTicket().getIdPersonCreator(),
                request.getTicket().getCreationDatetime(),
                request.getTicket().getTicketCloseDatetime()
        );
        TicketEntity sendToDatabase = ticketService.addTicket(obtainedInfo);
        BeanUtils.copyProperties(obtainedInfo, ticket);
        returnInfo.setTicket(ticket);
        return returnInfo;
    }
}