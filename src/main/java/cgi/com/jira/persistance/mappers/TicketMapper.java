package cgi.com.jira.persistance.mappers;

import cgi.com.jira.persistance.entities.TicketEntity;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TicketMapper implements RowMapper<TicketEntity> {

    @Override
    public TicketEntity mapRow(ResultSet resultSet, int i) throws SQLException {

        TicketEntity ticketEntity = new TicketEntity();
        ticketEntity.setId(resultSet.getLong("id"));
        ticketEntity.setName(resultSet.getString("name"));
        ticketEntity.setEmail(resultSet.getString("email"));
        ticketEntity.setIdPersonCreator(resultSet.getLong("id_person_creator"));
        ticketEntity.setIdPersonAssigned(resultSet.getLong("id_person_assigned"));
        ticketEntity.setCreationDateTime(resultSet.getTimestamp("creation_datetime").toLocalDateTime());
        ticketEntity.setTicketCloseDateTime(resultSet.getTimestamp("ticket_close_datetime").toLocalDateTime());
        return ticketEntity;
    }
}