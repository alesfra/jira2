package cgi.com.jira.persistance.repository;

import cgi.com.jira.persistance.entities.TicketEntity;
import cgi.com.jira.persistance.mappers.TicketMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class TicketRepository {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public TicketRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public TicketEntity getTicketById(Long id) {
        return (TicketEntity) jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE ticket.id = ?" +
                "", new Object[]{id}, new TicketMapper());
    }

    public TicketEntity getTicketByName(String name) {
        return (TicketEntity) jdbcTemplate.queryForObject("SELECT * FROM ticket WHERE ticket.name = ?" +
                "", new Object[]{name}, new TicketMapper());
    }

    public int newTicket(TicketEntity ticket) {
        return jdbcTemplate.update("INSERT INTO ticket (id, \"name\", email, " +
                        "id_person_creator, id_person_assigned, creation_datetime, ticket_close_datetime)" +
                        "VALUES (?, ?, ?, ?, ?, ?, ?); ",
                ticket.getId(),
                ticket.getName(),
                ticket.getEmail(),
                ticket.getIdPersonCreator(),
                ticket.getIdPersonAssigned(),
                ticket.getCreationDateTime(),
                ticket.getTicketCloseDateTime());
    }
}