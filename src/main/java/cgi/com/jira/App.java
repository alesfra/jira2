package cgi.com.jira;

import cgi.com.jira.soap.config.WebServiceConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(WebServiceConfig.class)
public class App {

    public static void main(String[] args) {
        SpringApplication.run(App.class, args);

    }
}